from flask import Flask, request, jsonify
from main import obtenerRespuesta, obtenerChatID
app = Flask(__name__)


# respuesta a peticion GET
@app.route('/')
def hello_world():
    return jsonify(username="magia")


# se define la respuesta a la peticion POST que envia la api de Telegram
@app.route('/', methods=['POST'])
def respuesta():
    req = request.get_json()
    print(req)
    id = obtenerChatID(req)
    msg = obtenerRespuesta(req)
    return jsonify(text=msg, method="sendMessage", chat_id=id)
