import requests
import re
import math
from settings import TOKEN
from pprint import pprint
from typing import Dict, Any


def validarTexto(update: Dict[str, Any]) -> bool:
    expresion = r'\s*([-]?[0-9]+)\s+([-]?[0-9]+)\s+([-]?[0-9]+)\s*'
    if re.match(expresion, update['text']) or update['text'] == "/start":
        return True
    else:
        return False


def validar(update: Dict[str, Any]) -> bool:
    if not update['message']:
        return False
    else:
        return validarTexto(update['message'])


def obtenerNumeros(entrada: str) -> [int]:
    # expresion = r'\s*([-]?[0-9]+.?([0-9]*))\s+([-]?[0-9]+.?([0-9]*))\s+([-]?[0-9]+.?([0-9]*))\s*'
    expresion = r'\s*([-]?[0-9]+)\s+([-]?[0-9]+)\s+([-]?[0-9]+)\s*'
    matches = re.match(expresion, entrada)
    print(matches.group(1))
    print(matches.group(2))
    print(matches.group(3))
    numeros = [int(matches.group(1)), int(matches.group(2)), int(matches.group(3))]
    return numeros


def resolverEcuacion(ecuacion: str) -> str:
    coeficientes = obtenerNumeros(ecuacion)
    print("a: " + str(coeficientes[0]) + " b: " + str(coeficientes[1]) + " c: " + str(coeficientes[2]))
    a = coeficientes[0]
    b = coeficientes[1]
    c = coeficientes[2]
    try:
        x1 = (-b + math.sqrt((b**2)-4*a*c))/(2*a)
        x2 = (-b - math.sqrt((b**2)-4*a*c))/(2*a)
        print("x1: " + str(x1) + " x2: " + str(x2))
        print("ECUACION RECIBIDA: " + ecuacion)
        return ("x1: " + str(x1) + "\nx2: " + str(x2))
    except ValueError:
        # return "Estamos+construyendo+el+bot....+proximamente+estara+funcionando"
        return "Error. No se puede sacar raiz negativa"


def enviarMsg(chat_id: str, msg: str):
    requests.post("https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s" % (TOKEN, chat_id, msg))


def obtenerRespuesta(update: Dict[str, Any]) -> str:
    # valida la existencia de la ecuacion
    if validar(update):
        if update['message']['text'] == "/start":
            return "Hola. Soy tu bot para resolver ecuaciones de segundo grado. Puedo resolver ecuaciones de segundo grado, simplemente escribe los coeficientes separados por espacios \nPor ejemplo: \npara x2 + 3x + 2 \nescribirias 1 3 2"
        else:
            respuesta = resolverEcuacion(update['message']['text'])
            return respuesta
    else:
        return "Error en el formato del mensaje \nFavor de escribir una ecuación. Por ejemplo: 1 1 5 para la ecuación x2 + x + 5"


def obtenerChatID(update: Dict[str, Any]):
    return update['message']['chat']['id']


def responder(update: Dict[str, Any]):
    respuesta = obtenerRespuesta(update)
    enviarMsg(update['message']['chat']['id'], respuesta)


def main():
    print("iniciando bot")
    # consulta a la api de telegram por updates
    req_updates = requests.get("https://api.telegram.org/bot%s/getUpdates" % TOKEN)
    # print(req_updates.text)
    # convierte la respuesta en un diccionario
    updates = req_updates.json()
    pprint(updates)
    print("CONTENIDO")
    for update in updates["result"]:
        responder(update)


if __name__ == "__main__":
    main()
