from main import obtenerNumeros


def test_obtenerNumeros():
    assert obtenerNumeros("1 3 2") == [1, 3, 2]
    assert obtenerNumeros("23  323 3") == [23, 323, 3]
    assert obtenerNumeros("-23  3 -23") == [-23, 3, -23]
